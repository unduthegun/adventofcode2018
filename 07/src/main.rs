use std::collections::HashMap;
use std::collections::HashSet;

const DEPS: &str = include_str!("input.txt");

fn main() {
    println!("Day 7: The Sum of Its Parts; First Star: {:?}", first_star());
    println!("Day 7: The Sum of Its Parts; Second Star: {:?}", second_star());
}

fn first_star() -> String  {
    let mut all = HashSet::new();
    let mut pending = HashSet::new();
    let mut unlock = HashMap::new();
    let mut locked = HashMap::new();

    for line in DEPS.lines() {
        let pre: char = line.chars().skip(5).next().unwrap();
        let pos: char = line.chars().skip(36).next().unwrap();

        pending.insert(pos);
        all.insert(pos);
        all.insert(pre);
        unlock.entry(pre).or_insert_with(|| HashSet::new()).insert(pos);
        locked.entry(pos).or_insert_with(|| HashSet::new()).insert(pre);
    }

    let mut ready: Vec<char> = all.difference(&pending).cloned().collect();
    let mut used: HashSet<char> = HashSet::new();
    let mut order = Vec::with_capacity(all.len());
    order.resize(all.len(), ' ');
    let mut ok = 0;

    let safeguard = HashSet::new();

   while !ready.is_empty() {
        ready.sort_by(|a, b| b.cmp(a));

        order[ok] = ready.pop().unwrap();
        used.insert(order[ok]);

        ready.extend(
            unlock.get(&order[ok]).unwrap_or(&safeguard).iter()
                .filter(|a| !used.contains(a))
                .filter(|b| locked.get(b).unwrap().iter().all(|a| used.contains(a)))
                .cloned().collect::<Vec<char>>());
        ok += 1;
    }
    order.iter().collect()
}

fn second_star() -> usize {
    0
}
