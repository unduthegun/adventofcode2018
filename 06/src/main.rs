use std::cmp;
use std::collections::HashMap;
use std::collections::HashSet;
use std::i32;

const COORDS: &'static str = include_str!("input.txt");

fn main() {
    println!("Day 6: Chronal Coordinates; First Star: {:?}", first_star());
    println!("Day 6: Chronal Coordinates; Second Star: {:?}", second_star());
}

fn top_bottom_right_left<'a>(coords: impl Iterator<Item = &'a(i32, i32)>) -> (i32, i32, i32, i32) {
    coords.fold(
        (i32::min_value(), i32::max_value(), i32::min_value(), i32::max_value()),
        |(t, b, l, r), &(x, y)| (cmp::max(t, x), cmp::min(b, x), cmp::max(l, y), cmp::min(r, y))
        )
}

fn first_star() -> i32 {
    let coords: Vec<(i32, i32)> = COORDS.lines()
        .map(|v| v.split(", "))
        .map(|mut v| (v.next().unwrap().parse().unwrap(), v.next().unwrap().parse().unwrap())).collect();

    let limits = top_bottom_right_left(coords.iter());
    let (t, b, r, l) = limits;

    let mut areas: HashMap<(i32, i32), i32> = HashMap::with_capacity(coords.len());
    let mut edgy: HashSet<(i32, i32)> = HashSet::new();

    for coord in coords.iter() {
        areas.insert(*coord, 0);
    }

    for x in b..(t + 1) {
        for y in l..(r + 1) {
            let closest = coords.iter().map(|&(xo, yo)| (xo - x).abs() + (yo - y).abs()).min().unwrap();
            let closest_points:Vec<&(i32, i32)> = coords.iter()
                .filter(|&(xo, yo)| (xo - x).abs() + (yo - y).abs() == closest)
                .collect();
            if closest_points.len() == 1 {
                *areas.get_mut(&closest_points[0]).unwrap() += 1;
            }
            if x == t || x == b || y == r || y == l {
                edgy.insert(*closest_points[0]);
            }
        }
    }

    for (x, y) in edgy {
        areas.remove(&(x, y));
    }

    *areas.values().max().unwrap()
}

fn second_star() -> u32 {
    let coords: Vec<(i32, i32)> = COORDS.lines()
        .map(|v| v.split(", "))
        .map(|mut v| (v.next().unwrap().parse().unwrap(), v.next().unwrap().parse().unwrap())).collect();

    let limits = top_bottom_right_left(coords.iter());
    let (t, b, r, l) = limits;

    let mut area_size = 0;

    for x in b..(t + 1) {
        for y in l..(r + 1) {
            let distance: i32 = coords.iter().map(|&(xo, yo)| (xo - x).abs() + (yo - y).abs()).sum();
            if distance < 10000 {
                area_size += 1;
            }
        }
    }

    area_size
}
