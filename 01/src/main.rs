use std::collections::HashSet;

const FREQS: &'static str = include_str!("input.txt");

fn main() {
    println!("Day 1: Chronal Calibration; First Star: {:?}", first_star());
    println!("Day 1: Chronal Calibration; Second Star: {:?}", second_star());
}

fn first_star() -> i32 {
    FREQS.split('\n')
        .map(|freq| freq.parse::<i32>().unwrap()).sum()
}

fn second_star() -> i32 {
    let mut freq = 0;
    let mut freqs: HashSet<i32> = HashSet::new();
    freqs.insert(freq.clone());

    for str_freq in FREQS.split('\n').cycle() {
        freq += str_freq.parse::<i32>().unwrap();

        let b_freq = freq.clone();
        if !freqs.insert(b_freq) {
            return b_freq;
        }
    }
    unreachable!()
}
