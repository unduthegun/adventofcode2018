use std::collections::HashSet;
use std::collections::LinkedList;
use std::iter::FromIterator;

const SEQ: &'static str = include_str!("input.txt");

fn main() {
    println!("Day 5: Alchemical Reduction; First Star: {:?}", first_star());
    println!("Day 5: Alchemical Reduction; Second Star: {:?}", second_star());
}

fn reduce(polymer: impl Iterator<Item = char>) -> usize {
    let mut processed: LinkedList<char> = LinkedList::new();

    for b in polymer {
        if ! processed.is_empty() &&
            ((b.is_lowercase() && processed.back().unwrap().is_uppercase()) ||
             (b.is_uppercase() && processed.back().unwrap().is_lowercase())) &&
            b.to_lowercase().zip(processed.back().unwrap().to_lowercase()).all(|(a, b)| a == b)
        {
            processed.pop_back();
        } else {
            processed.push_back(b);
        }
    }

    processed.len()
}

fn first_star() -> usize {
    reduce(SEQ.trim().chars())
}

fn second_star() -> usize {
    let characters: HashSet<char> = HashSet::from_iter(
        SEQ.trim().chars().map(|chr| chr.to_lowercase().next().unwrap()));
    characters.iter().
        map(|b| reduce(SEQ.trim().chars()
                       .filter(|a| a.to_lowercase().zip(b.to_lowercase())
                               .all(|(a, b)| a != b))))
        .min().unwrap()
}
